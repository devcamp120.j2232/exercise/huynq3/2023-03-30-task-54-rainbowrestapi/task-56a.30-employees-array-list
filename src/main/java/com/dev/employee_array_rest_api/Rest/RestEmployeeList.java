package com.dev.employee_array_rest_api.Rest;
import com.dev.employee_array_rest_api.Employee;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class RestEmployeeList {
    @GetMapping("/employees")
    public ArrayList<Employee> getEmployees() {
        Employee employee1 = new Employee(201, "Huy Nguyễn", "18+", 5000.0);
        Employee employee2 = new Employee(202, "Yến Vũ", "Children", 3000.0);
        Employee employee3 = new Employee(203, "Nhóc Võ", "Múa Lửa Nuốt Kiếm", 1000.0);

        System.out.println(employee1.toString());
        System.out.println(employee2.toString());
        System.out.println(employee3.toString());

        ArrayList<Employee> employees = new ArrayList<Employee>();
        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);

        return employees;
    }
}
